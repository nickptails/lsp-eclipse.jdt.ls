ARG ALPINE_VERSION
ARG JDK_VERSION

FROM alpine:${ALPINE_VERSION} AS downloader

ARG ECLIPSE_JDT_LS_SNAPSHOT
ARG ECLIPSE_JDT_LS_URL=https://www.eclipse.org/downloads/download.php?file=/jdtls/snapshots/jdt-language-server-${ECLIPSE_JDT_LS_SNAPSHOT}.tar.gz

RUN apk add --no-cache \
        curl \
        tar && \
    curl -fLo eclipse-jdt-ls.tar.gz ${ECLIPSE_JDT_LS_URL} && \
    mkdir -p /eclipse.jdt.ls && \
    tar xz --no-same-owner -f eclipse-jdt-ls.tar.gz -C /eclipse.jdt.ls


FROM amazoncorretto:${JDK_VERSION}-alpine${ALPINE_VERSION}

ARG ECLIPSE_JDT_LS_SNAPSHOT
ENV ECLIPSE_JDT_LS_SNAPSHOT=${ECLIPSE_JDT_LS_SNAPSHOT}
COPY --from=downloader /eclipse.jdt.ls/ /app/java/eclipse.jdt.ls/

CMD ["sh", "-c", "java -jar /app/java/eclipse.jdt.ls/plugins/org.eclipse.equinox.launcher_${ECLIPSE_JDT_LS_SNAPSHOT}.jar"]
